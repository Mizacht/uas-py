from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route('/')
def home():
    # return 'Hello, Flask'
    return render_template(
        'home.html'
    )

@app.route('/services')
def services():
    # return 'services page'
    return render_template(        
        'services.html'
    )

@app.route('/government')
def government():
    return render_template(
        'government.html'
    )

@app.route('/contact')
def contact():
    return render_template(
        'contact.html'
    )

@app.route('/news')
def news():
    return render_template(
        'news.html'
    )

@app.route('/services/pariwisata')
def pariwisata():
    return render_template(
        'pariwisata.html'
    )

@app.route('/lokasi')
def lokasi():
    return render_template(
        'lokasi.html'
    )